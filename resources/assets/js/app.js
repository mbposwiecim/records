(function($) {
    // code using $ as alias to jQuery
    $(function() {
        console.log('DOM is ready');

        // side navigation on mobile
        $('.button-collapse').sideNav({
            edge: 'right'
        });

        // admin dropdown menu
        $(".dropdown-button").dropdown();

        // don't close chips in card view
        $('.card-action').on("click", ".chip .material-icons", function(event) {
            event.stopPropagation();
        });

        // close card-reveal when opening another one
        $('.activator').on('click change', function() {
          $('.card-reveal').hide({
            'duration': 225,
            'easing': 'easeInOutQuad'
          });
        });

        // date picker, see docs: http://amsul.ca/pickadate.js/date/
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 3, // Creates a dropdown of 15 years to control year
            // date picker translation
            labelMonthNext: 'Następny miesiąc',
            labelMonthPrev: 'Poprzedni miesiąc',
            labelMonthSelect: 'Wybierz miesiąc',
            labelYearSelect: 'Wybierz rok',
            monthsFull: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
            monthsShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
            weekdaysFull: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
            weekdaysShort: ['Nie', 'Pon', 'Wto', 'Śro', 'Czw', 'Pią', 'Sob'],
            weekdaysLetter: ['N', 'P', 'W', 'Ś', 'C', 'P', 'S'],
            today: 'Dziś',
            clear: 'Wyczyść',
            close: 'OK',
            // format
            format: 'd mmmm, yyyy',
            formatSubmit: 'yyyy-mm-dd',
            hiddenName: true,
            hiddenSuffix: ''
        });

        // materialize select element
        $('select').material_select();

        // setup submit listener
        $('#archive-form').submit(function(event) {
            event.preventDefault(); // disable form submit

            // spinner start
            $('#modal-progress').openModal({
                dismissible: false,
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });

            var formData = new FormData($(this)[0]);

            $.ajax({
                type: 'POST',
                url:  $(this).attr('action'),
                cache: false,
                contentType: false, //tell jquery to avoid some checks
                processData: false,
                data: formData,
                success: function(data) {
                    var delay = 3000;
                    $('#archive-form')[0].reset();
                    Materialize.toast(data.responseText, delay);
                    setTimeout(function(){ window.location = '/'; }, delay);
                },
                error: function(response) {
                    var errors = $.parseJSON(response.responseText);
                    var toasMessage = '';
                    $.each(errors, function(field, message) {
                        toasMessage += '<p>' + message + '</p>';
                    });
                    Materialize.toast(toasMessage, 5000);
                },
                complete: function(jqXhr) {
                    // after success & error → stop spinner
                    $('#modal-progress').closeModal();
                }
            });
        });

    }); // end of document ready
})(jQuery); // end of jQuery name space
