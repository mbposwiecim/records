<div id="breadcrumbs" class="section">
    <nav>
        <div class="nav-wrapper white">
            <div class="col s12">
                {{ link_to('/', 'Strona główna', ['class' => 'breadcrumb']) }}
                {{ isset($archive) ? link_to_action('ArchiveController@show', $archive->title, $archive->slug, ['class' => 'breadcrumb']) : '' }}
            </div>
        </div>
    </nav>
</div>
