<div class="col s12 m6 l4">
    <div id="archive-card">
        <div class="card medium hoverable">
            <div class="card-image waves-effect waves-block waves-light">
                @if ($archive->imagepath && File::exists('archive/'.$archive->imagepath))
                <img class="activator" src="/archive/{{ $archive->imagepath }}">
                @else
                <img class="activator" src="https://placekitten.com/400/250">
                @endif
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">{{ $archive->title }}</span>
            </div>
            <div class="card-action row">
                <div class="col s5">
                {{ link_to_action('ArchiveController@show', 'Otwórz', [$archive->slug], ['class' => 'btn blue-grey waves-effect']) }}
                </div>
                <div class="col s7">
                    <span class="stats-wrapper right">
                        <div class="chip played tooltipped" data-tooltip="Odsłuchano">
                            <i class="material-icons">play_arrow</i>{{ $archive->played }}
                        </div>
                        <div class="chip downloaded tooltipped" data-tooltip="Pobrano">
                            <i class="material-icons">file_download</i>{{ $archive->downloaded }}
                        </div>
                    </span>
                </div>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">{{ $archive->title }}<i class="material-icons right">close</i></span>
                <p class="category">Kategoria: {{ $archive->category['name'] ?: 'Brak' }}</p>
                <p class="">{{ $archive->description }}</p>
                <ul class="timestamps">
                    <li><i class="tiny material-icons">surround_sound</i> wyemitowano<span class="badge">{{ $archive->aired }}</span></li>
                    <li><i class="tiny material-icons">file_upload</i> przesłano<span class="badge">{{ $archive->published }}</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
