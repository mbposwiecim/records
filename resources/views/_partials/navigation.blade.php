<div class="navbar-fixed">
    <nav class="blue-grey darken-1" role="navigation">
        <div class="nav-wrapper container">
            <a id="logo-container" href="{{ action('ArchiveController@index') }}" class="brand-logo left"><img src="/img/logo.png" alt="Logo" /></a>
            <ul class="right hide-on-med-and-down">
                <li>
                    {!! Form::open(['method'=>'GET', 'action' => isset($post_url) ? $post_url : 'ArchiveController@index', 'class'=>'search-form', 'role'=>'zapytanie']) !!}
                    <div class="input-field">
                        <label for="s"><i class="material-icons">search</i></label>
                        {!! Form::text('zapytanie', isset($search) ? $search : '', ['id' => 'query', 'autocomplete' => 'off', 'placeholder' => 'Szukaj...', 'required']) !!}
                    </div>
                    {!! Form::close() !!}
                </li>
                <li><a class="dropdown-button" href="" data-activates="categories-dropdown">Kategoria<i class="material-icons right">arrow_drop_down</i></a></li>
                @if (Auth::check())
                <li><a class="dropdown-button" href="" data-activates="admin-dropdown">Administracja<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a href="{{ action('Auth\AuthController@logout') }}">Wyloguj</a></li>
                @else
                <li><a href="{{ action('Auth\AuthController@showLoginForm') }}">Zaloguj</a></li>
                @endif
            </ul>
            <ul id="admin-dropdown" class="dropdown-content">
                <li><a href="{{ action('Admin\ArchiveController@index') }}">Audycje</a></li>
                <li><a href="{{ action('Admin\CategoryController@index') }}">Kategorie</a></li>
            </ul>
            @if (isset($categories))
            <ul id="categories-dropdown" class="right dropdown-content">
                <li><a href="{{ action('ArchiveController@index') }}" class="blue-grey-text text-darken-2">Wszystkie</a></li>
                <li class="divider"></li>
                @foreach ($categories as $category)
                <li><a href="{{ action('ArchiveController@category', $category->slug) }}" class="blue-grey-text text-darken-2">{{ $category->name }}</a></li>
                @endforeach
            </ul>
            @endif
            <ul id="nav-mobile" class="side-nav">
                @if (Auth::check())
                <li><a href="{{ action('Auth\AuthController@logout') }}">Wyloguj</a></li>
                @else
                <li><a href="{{ action('Auth\AuthController@showLoginForm') }}">Zaloguj</a></li>
                @endif
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse right"><i class="material-icons">menu</i></a>
        </div>
    </nav>
</div>
