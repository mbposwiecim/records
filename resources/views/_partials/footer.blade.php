<footer class="page-footer blue-grey darken-1">
    <div class="container">
        <div class="row">
            <div class="col l8 s12">
                <h5 class="white-text">Radio Oświęcim | Archiwum</h5>
                <p class="grey-text text-lighten-4">Społecznie, muzycznie, na bieżąco - dla Was - Radio Oświęcim</p>
            </div>
            <div class="col l4 s12">
                <ul>
                    <li><a class="grey-text text-lighten-3" href="http://radiooswiecim.pl"><i class="fa fa-external-link-square" aria-hidden="true"></i> Radio Oświęcim</a></li>
                    <li><a class="grey-text text-lighten-3" href="http://facebook.com/RadioOswiecim"><i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook</a></li>
                    <li><a class="grey-text text-lighten-3" href="http://twitter.com/radiooswiecim"><i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter</a></li>
                    <li><a class="grey-text text-lighten-3" href="http://mbp-oswiecim.pl"><i class="fa fa-external-link-square" aria-hidden="true"></i> MBP Oświęcim</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2016 Radio Oświęcim
        </div>
    </div>
    @if (Auth::check())
    <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
        <a class="btn-floating btn-large red tooltipped" data-position="left" href="{{ action('ArchiveController@create') }}" data-tooltip="Dodaj nowe archiwum">
            <i class="large material-icons">add</i>
        </a>
    </div>
    @endif
</footer>
