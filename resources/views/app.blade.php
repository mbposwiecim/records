<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>Radio Oświęcim | Archiwum @yield('title')</title>
    <!--Import Google Icon Font-->
    {!! Html::style('http://fonts.googleapis.com/icon?family=Material+Icons') !!}
    <!--Import materialize.css-->
    {!! Html::style('css/materialize.min.css') !!}
    <!--Import local css-->
    {!! Html::style('css/app.css') !!}
    @yield('extra_css')
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
</head>
<body>
    @include('_partials.navigation')
    <main>
        <div class="container">
            @yield('content')
        </div>
    </main>
    @include('_partials.footer')
    <!--Import jQuery before materialize.js-->
    {!! Html::script('js/jquery.min.js') !!}
    {!! Html::script('js/materialize.min.js') !!}
    {!! Html::script('https://use.fontawesome.com/785c80ba42.js') !!}
    {!! Html::script('js/app.js') !!}
    @yield('extra_js')
</body>
</html>
