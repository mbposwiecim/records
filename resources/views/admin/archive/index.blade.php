@extends('app')

@section('content')
<div class="row section">
    <table class="bordered highlight responsive-table">
        <thead>
            <tr>
                <th data-field="title">Tytuł</th>
                <th data-field="aired">Wyemitowano</th>
                <th data-field="published">Przesłano</th>
                <th data-field="action">Zarządzaj</th>
                <th data-field="downloaded">P</th>
                <th data-field="played">O</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($archives as $key => $archive)
            <tr>
                <td>{{ $archive->title }}</td>
                <td>{{ $archive->aired }}</td>
                <td>{{ $archive->published }}</td>
                <td>
                    {!! link_to_action('ArchiveController@show', 'launch', [$archive->slug], ['class' => 'open blue-grey-text text-darken-2 material-icons tooltipped', 'data-tooltip' => 'Pokaż']) !!}
                    {!! link_to_action('ArchiveController@edit', 'mode_edit', [$archive->slug], ['class' => 'edit blue-grey-text text-darken-2 material-icons tooltipped', 'data-tooltip' => 'Edytuj']) !!}
                    {!! Form::open([
                        'method' => 'delete',
                        'action' => ['ArchiveController@destroy', $archive->slug],
                        'class' => 'delete-archive-form'
                      ])!!}
                    {!! Form::submit('delete', ['class' => 'delete blue-grey-text text-darken-2 material-icons tooltipped', 'data-tooltip' => 'Usuń', 'onClick' => 'return confirm("Na pewno usunąć ?")']) !!}
                    {!! Form::close() !!}
                </td>
                <td>
                    <div class="chip played"><i class="material-icons">play_arrow</i>{{ $archive->downloaded }}</div>
                </td>
                <td>
                    <div class="chip downloaded"><i class="material-icons">file_download</i>{{ $archive->played }}</div>
                </td>
            </tr>
        @endforeach
        </tbody>
      </table>
</div>
<div class="row section center">
{!! (new Landish\Pagination\Materialize($archives))->render() !!}
</div>
@endsection
