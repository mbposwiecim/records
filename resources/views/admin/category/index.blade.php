@extends('app')

@section('content')
<div class="row section">
    <div class="col s12 m8 offset-m2">
        <div id="categories">
            <ul class="collection with-header">
                <li class="collection-header center"><div>Kategorie<a href="#category-modal" class=" add-category secondary-content"><i class="material-icons">add_box</i></a></div></li>
                @foreach($categories as $category)
                  <li id="item-{{ $category->id }}" class="collection-item">
                      <div>{!! $category->name !!}
                          <a href="#!" class="delete-category secondary-content" data-id="{{ $category->id }}"><i class="material-icons">delete</i></a>
                          <a href="#!" class="edit-category secondary-content" data-id="{{ $category->id }}"><i class="material-icons">mode_edit</i></a>
                      </div>
                  </li>
                @endforeach
            </ul>
        </div>
        <div id="category-modal" class="modal">
            <div class="modal-content">
                <h5>Kategoria</h5>
                {!! Form::open(['id' => 'category-form', 'action' => 'Admin\CategoryController@store']) !!}
                <div class="row">
                    <div class="input-field col s12">
                    {!! Form::label('name', 'Nazwa kategorii') !!}
                    {!! Form::text('name', null, ['id' => 'name', 'class' => '', 'required', 'minlength' => '5', 'maxlength' => '255']) !!}
                    {!! Form::hidden('id', 0, ['id' => 'id']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <a href="#!" class=" modal-action modal-close waves-effect waves-dark blue-grey btn">Anuluj</a>
                        {!! Form::submit('Zapisz', ['class' => 'save-category waves-effect waves-dark btn right']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra_js')
<script type="text/javascript">
$(function() {
    var url = '/administracja/kategorie/';
    var type = 'POST';

    // setup ajax
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
    });

    // open modal to create new category
    $('.add-category').on('click', function() {
        type = 'POST';
        $('#category-form').trigger('reset');
        $('#category-modal').openModal({
            dismissible: false,
            ready: function() {
                $('#name').focus();
            },
        });
    });

    // save category
    $('.save-category').on('click', function(event) {
        event.preventDefault();

        var name = $('#name').val();
        var id = $('#id').val();
        $.ajax({
            type: type,
            data: $('form').serialize(),
            url: url,
            success: function(data) {

                var item = '<li id="item-' + data.id + '" class="collection-item">' +
                '<div>' + data.name +
                '<a href="#!" class="delete-category secondary-content" data-id="' + data.id + '"><i class="material-icons">delete</i></a>' +
                '<a href="#!" class="edit-category secondary-content" data-id="' + data.id + '"><i class="material-icons">mode_edit</i></a>' +
                '</div>' +
                '</li>';

                if (type == 'POST') {
                    // after adding new
                    $("#categories ul").append(item);
                }
                if(type == 'PATCH') {
                    // aftr editing
                    $("#item-" + id).replaceWith(item);
                }

                $('#category-form').trigger('reset');
                $('#category-modal').closeModal();
            },
            error: function(data) {
                console.error(data);
            },
        });
    });

    // edit categories
    $('#categories ul').on('click', '.edit-category', function() {
        type = 'PATCH';
        var id = $(this).data('id');
        url = '/administracja/kategorie/' + id;

        $.get('/administracja/kategorie/' + id, function(data) {
            $('#name').val(data.name);
            $('#id').val(data.id);
            $('#category-modal').openModal({
                dismissible: false,
                ready: function() {
                    $('#name').focus();
                },
            });
        });

    });

    // removing categories
    $('#categories ul').on('click', '.delete-category', function(event) {
        event.preventDefault();
        if(confirm("Na pewno usunąć?")) {
            type = 'DELETE';
            var id = $(this).data('id');
            url = '/administracja/kategorie/' + id;

            $.ajax({
                type: type,
                url: url,
                success: function (data) {
                    $("#item-" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });
});
</script>
@endsection
