@extends('app')

@section('title')
  - nowa audycja
@endsection

@section('content')
    <div class="welcome-wrapper">
        <h4 class="center-align">Dodaj nową audycję</h4>
    </div>
    <div class="row errors">
        <div class="col s12 m6 offset-m3">
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="dismissable">{{ $error }}</p>
            @endforeach
        @endif
      </div>
    </div>
    <div class="section">
        {!! Form::open(['id' => 'archive-form', 'action' => 'ArchiveController@store', 'files' => true]) !!}
        <div class="row">
            <div class="input-field col s12 m6 offset-m3">
            {!! Form::label('title', 'Nazwa audycji') !!}
            {!! Form::text('title', null, ['class' => '', 'required', 'minlength' => '10', 'maxlength' => '255']) !!}
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 m6 offset-m3">
            {!! Form::label('description', 'Opis audycji') !!}
            {!! Form::textarea('description', null, ['class' => 'materialize-textarea', 'required', 'minlength' => '30']) !!}
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 m3 offset-m3">
            {!! Form::label('aired', 'Data emisji') !!}
            {!! Form::date('aired', '', ['class' => 'datepicker', 'required']) !!}
            </div>
            <div class="input-field col s12 m3">
            {!! Form::label('published', 'Data publikaji') !!}
            {!! Form::date('published', '', ['class' => 'datepicker', 'data-value' => Carbon\Carbon::now(), 'required']) !!}
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 m6 offset-m3">
            {!! Form::select('category_id', $options, null, ['placeholder' => 'Bez kategorii']) !!}
            {!! Form::label('category_id', 'Kategoria') !!}
            </div>
        </div>
        <div class="row">
            <div class="file-field input-field col s12 m6 offset-m3">
                <div class="btn grey">
                    <span>Plakat</span>
                    {!! Form::file('image', ['id' => 'image', 'class' => '', 'accept' => '.jpg,.png', 'required']) !!}
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path" type="text">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="file-field input-field col s12 m6 offset-m3">
                <div class="btn grey">
                    <span>Plik audio</span>
                    {!! Form::file('audio', ['id' => 'audio', 'class' => '', 'accept' => '.mp3', 'required']) !!}
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path" type="text">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m6 offset-m3">
                {!! Form::submit('Zapisz', ['class' => 'waves-effect waves-light btn right']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    <div class="section">
        <div id="modal-progress" class="modal small">
            <div class="modal-content">
                <h5>Przesyłanie plików...</h5>
                <div class="preloader-wrapper big active center">
                    <div class="spinner-layer spinner-blue-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
