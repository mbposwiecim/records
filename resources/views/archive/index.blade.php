@extends('app')

@section('content')
<div class="row section welcome-wrapper">
    <div class="col s12">
        <h4 class="center-align">Audycje Radia Oświęcim</h4>
        {!! isset($search) ? '<p class="center-align">Wynik wyszukiwania dla: ' . $search . '</p>' : '' !!}
        {!! isset($selectedCategory) ? '<p class="center-align">Kategoria: ' . $selectedCategory->name . '</p>' : '' !!}
    </div>
</div>
<div class="row section">
@foreach ($archives as $key => $archive)
    @include('_partials.card', compact('archive'))
@endforeach
</div>
{{-- TODO What about url scheme like /strona/3 instead ?page=3 --}}
<div class="row section center">
    {!! isset($search) ? $archives->appends(['zapytanie' => $search])->render(new Landish\Pagination\Materialize($archives)) : (new Landish\Pagination\Materialize($archives))->render() !!}
</div>
@endsection
