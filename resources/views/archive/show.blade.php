@extends('app')

@section('title')
  - {{ $archive->title }}
@endsection

@section('extra_css')
<!--Import local css-->
{!! Html::style('css/player.css') !!}
@endsection

@section('extra_js')
{!! Html::script('js/amplitude.min.js') !!}
<script type="text/javascript">
Amplitude.init({
    'songs': [{
        'name': '{{ $archive->title }}',
        'artist': 'Radio Oświęcim',
        'url': '/odtworz/{{ $archive->slug }}'
    }],
    'volume': 1,
    'autoplay': false,
    'callbacks': {
        'after_init': 'afterInit'
    }
});

function afterInit() {
    console.log('after init callback');
};

$(document).ready(function() {
    $('.amplitude-play-pause').one('click', function() {
        $.post({
            url: '/stats',
            data: {
                value: '{{ $archive->id }}',
                _token: '{{ csrf_token() }}'
            }
        });
    });

    $('#play-pause').click(function() {
        var current = $(this).children('.icon').text();
        if (current == 'play_arrow') {
            $(this).children('.icon').text('pause');
        }
        if (current == 'pause') {
           $(this).children('.icon').text('play_arrow');
        }
    });
});
</script>
@endsection

@section('content')
<div class="welcome-wrapper">
    <h3 class="center-align">{{ $archive->title }}</h3>
</div>
@include('_partials.breadcrumbs', ['archive' => $archive])
<div id="archive-content" class="row section">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <div class="col l3">
                        <div class="card-image">
                            @if ($archive->imagepath && File::exists('archive/'.$archive->imagepath))
                            <img src="/archive/{{ $archive->imagepath }}">
                            @else
                            <img src="https://placekitten.com/400/250">
                            @endif
                        </div>
                    </div>
                    <div class="col l9">
                        <div class="description">
                            <p>{{ $archive->description }}</p>
                            <p class="category">Kategoria: {{ $archive->category['name'] ?: 'Brak' }}</p>
                        </div>
                        <div id="player" class="player-row center">
                            <div class="metainfo">
                                <span amplitude-song-info="artist"></span> prezentuje: <span amplitude-song-info="name"></span>
                            </div>
                            <div class="time">
                              <span class="amplitude-current-minutes" amplitude-single-current-minutes="true">0</span>:<span class="amplitude-current-seconds" amplitude-single-current-seconds="true">00</span> /
                              <span class="amplitude-duration-minutes" amplitude-single-duration-minutes="true">0</span>:<span class="amplitude-duration-minutes" amplitude-single-duration-seconds="true">00</span>
                            </div>
                            <a id="play-pause" class="amplitude-play-pause amplitude-paused btn-floating btn-large waves-effect waves-light red"><i class="icon material-icons">play_arrow</i></a>
                            <div id="seek-field">
                                <input type="range" class="amplitude-song-slider" amplitude-singular-song-slider="true" value="0">
                            </div>
                            <div id="volume-up" class="">
                                <i class="material-icons">volume_up</i>
                            </div>
                            <div id="volume-field">
                                <input type="range" class="amplitude-volume-slider" value="0"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-action row">
                <div class="col s12 m6">
                    {{ link_to_action('ArchiveController@download', 'Pobierz', [$archive->slug], ['class' => 'btn blue-grey waves-effect']) }}
                    <span class="stats-wrapper center">
                        <div class="chip played tooltipped" data-tooltip="Odsłuchano">
                            <i class="material-icons">play_arrow</i>{{ $archive->played }}
                        </div>
                        <div class="chip downloaded tooltipped" data-tooltip="Pobrano">
                            <i class="material-icons">file_download</i>{{ $archive->downloaded }}
                        </div>
                    </span>
                </div>
                <div class="col s12 m6">
                    <div class="right">
                        <!-- AddToAny BEGIN -->
                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_google_plus"></a>
                        </div>
                        <script async src="https://static.addtoany.com/menu/page.js"></script>
                        <!-- AddToAny END -->
                    </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
