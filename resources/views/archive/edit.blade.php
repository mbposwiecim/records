@extends('app')

@section('title')
  - edycja audycji {{ $archive->title }}
@endsection

@section('content')
    <div class="welcome-wrapper">
        <h4 class="center-align">Dodaj nową audycję</h4>
    </div>
    <div class="row errors">
        <div class="col s12 m6 offset-m3">
        @if ($errors->any())
            <ul class="collection">
                @foreach ($errors->all() as $error)
                    <li class="collection-item">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
      </div>
    </div>
    <div class="section">
        {!! Form::model($archive, ['method' => 'PATCH', 'action' => ['ArchiveController@update', $archive->slug], 'files' => true]) !!}
        <div class="row">
            <div class="input-field col s12 m6 offset-m3">
            {!! Form::label('title', 'Nazwa audycji') !!}
            {!! Form::text('title', null, ['class' => '', 'required']) !!}
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 m6 offset-m3">
            {!! Form::label('description', 'Opis audycji') !!}
            {!! Form::textarea('description', null, ['class' => 'materialize-textarea', 'required']) !!}
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 m3 offset-m3">
            {!! Form::label('aired', 'Data emisji') !!}
            {!! Form::date('aired', '', ['class' => 'datepicker', 'data-value' => $archive->aired, 'required']) !!}
            </div>
            <div class="input-field col s12 m3">
            {!! Form::label('published', 'Data publikaji') !!}
            {!! Form::date('published', '', ['class' => 'datepicker', 'data-value' => Carbon\Carbon::now(), 'required']) !!}
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 m6 offset-m3">
            {!! Form::select('category_id', $options, null, ['placeholder' => 'Bez kategorii']) !!}
            {!! Form::label('category_id', 'Kategoria') !!}
            </div>
        </div>
        <div class="row">
            <div class="col s12 m6 offset-m3">
                {!! Form::submit('Zapisz', ['class' => 'waves-effect waves-light btn right']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
