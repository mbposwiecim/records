@extends('app')

@section('content')
<div class="welcome-wrapper">
    <h3 class="center-align">Zaloguj się</h3>
</div>
<div class="row errors">
    <div class="col s12 m6 offset-m3">
    @if ($errors->any())
        <ul class="collection">
            @foreach ($errors->all() as $error)
                <li class="collection-item">{{ $error }}</li>
            @endforeach
        </ul>
    @endif
  </div>
</div>
<div class="section">
    {!! Form::open(['action' => 'Auth\AuthController@login']) !!}
    <div class="row">
        <div class="input-field col s12 m6 offset-m3">
            {!! Form::label('email', 'Adres email') !!}
            {!! Form::email('email', null, ['class' => 'validate']) !!}
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12 m6 offset-m3">
            {!! Form::label('password', 'Hasło') !!}
            {!! Form::password('password', null, ['class' => '']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6 offset-m3">
            {!! Form::checkbox('remember', 'on', false, ['id' => 'remember', 'class' => 'filled-in']) !!}
            {!! Form::label('remember', 'Zapamiętaj') !!}
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6 offset-m3">
            {!! Form::submit('Zaloguj', ['class' => 'btn blue-grey waves-effect left inline']) !!}
            {!! link_to_action('Auth\PasswordController@showResetForm', 'Nie pamiętasz hasła?', '', ['class' => 'btn blue-grey waves-effect right']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
