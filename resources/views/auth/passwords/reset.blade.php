@extends('app')

@section('content')
<div class="welcome-wrapper">
    <h5 class="center-align">Resetuj hasło</h5>
</div>
<div class="row errors">
    <div class="col s12 m6 offset-m3">
    @if ($errors->any())
        <ul class="collection">
            @foreach ($errors->all() as $error)
                <li class="collection-item">{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    </div>
</div>
<div class="section">
    {!! Form::open(['action' => 'Auth\PasswordController@reset']) !!}
    {!! Form::hidden('token', $token) !!}
    <div class="row">
        <div class="input-field col s12 m6 offset-m3">
        {!! Form::label('email', 'Adres email') !!}
        {!! Form::email('email', $email, ['class' => 'validate']) !!}
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12 m6 offset-m3">
        {!! Form::label('password', 'Hasło') !!}
        {!! Form::password('password', null, ['class' => '']) !!}
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12 m6 offset-m3">
        {!! Form::label('password_confirmation', 'Powtórz hasło') !!}
        {!! Form::password('password_confirmation', null, ['class' => '']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6 offset-m3">
            {!! Form::submit('Resetuj hasło', ['class' => 'btn blue-grey waves-effect waves-dark left']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
