@extends('app')

@section('content')
<div class="welcome-wrapper">
    <h5 class="center-align">Resetuj hasło</h5>
</div>
<div class="row errors">
    <div class="col s12 m6 offset-m3">
    @if (session('status'))
        {{-- TODO Display proper session alert --}}
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if ($errors->any())
        <ul class="collection">
            @foreach ($errors->all() as $error)
                <li class="collection-item">{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    </div>
</div>
<div class="section">
    {!! Form::open(['action' => 'Auth\PasswordController@sendResetLinkEmail']) !!}
    <div class="row">
        <div class="input-field col s12 m6 offset-m3">
        {!! Form::label('email', 'Adres email') !!}
        {!! Form::email('email', old('email'), ['class' => 'validate']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6 offset-m3">
            {!! Form::submit('Resetuj hasło', ['class' => 'btn blue-grey waves-effect left']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
