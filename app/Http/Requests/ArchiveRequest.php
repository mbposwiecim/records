<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArchiveRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // auth checking is done in routes.php
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                $rules['title'] = 'bail|required|unique:archives|min:10|max:255';
                break;
            case 'PATCH':
                $rules['title'] = 'bail|required|min:10|max:255';
                break;
            default:
                break;
        }
        $rules['description'] = 'bail|required|min:30';
        $rules['aired'] = 'bail|required|date_format:Y-m-d';
        $rules['published'] = 'bail|required|date_format:Y-m-d';

        return $rules;
    }
}
