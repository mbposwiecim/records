<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ArchiveController@index');
Route::get('audycja/nowa', ['middleware' => 'auth', 'uses' => 'ArchiveController@create']);
Route::get('audycja/{slug}', 'ArchiveController@show');
Route::post('audycja', ['middleware' => 'auth', 'uses' => 'ArchiveController@store']);
Route::get('audycja/{slug}/edytuj', ['middleware' => 'auth', 'uses' => 'ArchiveController@edit']);
Route::patch('audycja/{slug}', ['middleware' => 'auth', 'uses' => 'ArchiveController@update']);
Route::delete('audycja/{slug}', ['middleware' => 'auth', 'uses' => 'ArchiveController@destroy']);

Route::get('kategoria/{slug}', 'ArchiveController@category');

// routes for playing and downloading audio file
Route::get('odtworz/{slug}', 'ArchiveController@play');
Route::post('stats', 'ArchiveController@updatePlayedStats');
Route::get('pobierz/{slug}', 'ArchiveController@download');

// auth
Route::get('zaloguj','Auth\AuthController@showLoginForm');
Route::post('zaloguj', 'Auth\AuthController@login');
Route::get('wyloguj', 'Auth\AuthController@logout');

// enable when creating a new user
// Route::get('zarejestruj', 'Auth\AuthController@showRegistrationForm');
// Route::post('zarejestruj', 'Auth\AuthController@register');

Route::get('haslo/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('haslo', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('haslo/resetuj', 'Auth\PasswordController@reset');


/*
| Admin routing
*/
Route::group(['prefix' => 'administracja', 'namespace' => 'Admin', 'middleware' => 'auth'], function() {
    Route::get('audycje', 'ArchiveController@index');
    Route::get('kategorie', 'CategoryController@index');
    Route::get('kategorie/{id}', 'CategoryController@show');
    Route::patch('kategorie/{id}', 'CategoryController@update');
    Route::post('kategorie', 'CategoryController@store');
    Route::delete('kategorie/{id}', 'CategoryController@destroy');
});
