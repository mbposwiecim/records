<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ArchiveRequest;
use App\Archive;
use App\Category;
use Storage;
use File;
use Image;

class ArchiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::all();
        if($request->has('zapytanie')) {
            $search = $request->input('zapytanie');
            $archives = Archive::where('title', 'like', '%'.$search.'%')
                ->orWhere('description', 'like', '%'.$search.'%')
                ->orderBy('title')
                ->paginate(9);
        } else {
            $archives = Archive::orderBy('aired', 'desc')->paginate(9);
        }
        return view('archive.index', compact('archives', 'search', 'categories'));
    }

    /**
     * Display archives from specified category
     *
     * @param type var Description
     * @return {11:return type}
     */
    public function category($slug)
    {
        $categories = Category::all();
        $selectedCategory = Category::findBySlug($slug);
        $archives = $selectedCategory->archives()->orderBy('aired', 'desc')->paginate(9);
        return view('archive.index', compact('archives', 'categories', 'selectedCategory'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $categories = Category::all();
        $archive = Archive::findBySlug($slug);
        return view('archive.show', compact('archive', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $options = $categories->lists('name', 'id');
        return view('archive.create', compact('options', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ArchiveRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArchiveRequest $request)
    {
        $archive = new Archive([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'aired' => $request->get('aired'),
            'published' => $request->get('published'),
            'category_id' => $request->get('category_id')
        ]);
        $archive->save();

        $disk = Storage::disk('archive');
        $template = $archive->id.'/'.md5(time());

        if($request->hasFile('audio'))
        {
            $audio_file = $request->file('audio');
            $audio_file_name = $template.'.'.$audio_file->getClientOriginalExtension();
            if ($audio_file->isValid())
            {
                $disk->put($audio_file_name, File::get($audio_file->getRealPath()));
                $archive->filepath = $audio_file_name;
                $archive->save();
            }
        }
        if($request->hasFile('image'))
        {
            $image_file = $request->file('image');
            $image_file_name = $template.'.'.$image_file->getClientOriginalExtension();
            if($image_file->isValid())
            {
                $resized = Image::make($image_file)->fit(420, 240, function($constraint) {
                    $constraint->upsize();
                });
                $disk->put($image_file_name, $resized->stream());
                $archive->imagepath = $image_file_name;
                $archive->save();
            }
        }

        $response = ['responseText' => 'Archiwum zostało dodane!'];
        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $archive = Archive::findBySlug($slug);
        $categories = Category::all();
        $options = $categories->lists('name', 'id');
        return view('archive.edit', compact('archive', 'options', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ArchiveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArchiveRequest $request, $slug)
    {
        // TODO Allow to change image/audio file?
        $archive = Archive::findBySlug($slug);
        $archive->update($request->all());
        return redirect()->action('ArchiveController@show', $archive->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $archive = Archive::findBySlug($slug);
        Archive::destroy($archive->id);
        Storage::disk('archive')->deleteDirectory($archive->id); # removes files too
        return redirect('administracja')->with('message', 'Audycja została usunięta. ' . $archive->title);
    }

    /**
     * Handle playing stats and return requested audio file itself.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function play($slug)
    {
        $archive = Archive::findBySlug($slug);
        $filename = $archive->filepath;

        $disk = Storage::disk('archive');
        $mime = $disk->mimeType($filename);
        $size = $disk->size($filename);

        $headers = [
            'Content-Type' => $mime,
            'Content-Length' => $size,
            'Accept-Ranges' => 'bytes',
        ];
        $path = $this->getStoragePath($disk).$filename;
        return response()->file($path, $headers);
    }

    /**
     * Handle downloading stats and return requested audio file itself.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download($slug)
    {
        $archive = Archive::findBySlug($slug);
        $filename = $archive->filepath;

        $disk = Storage::disk('archive');
        $path = $this->getStoragePath($disk).$filename;
        $target = 'Radio Oświęcim - '.$archive->aired.' - '.$archive->title.'.'.File::extension($path);
        if(file_exists($path)) {
            // update stats
            $archive->downloaded += 1;
            $archive->save();
        }
        return response()->download($path, $target);
    }

    /**
     * Get storage disk path
     *
     * @param \Illuminate\Filesystem\FilesystemAdapter $disk
     * @return string
     */
    private function getStoragePath($disk)
    {
        return $disk->getDriver()->getAdapter()->getPathPrefix();
    }

    /**
     * Update stats for played tracks
     *
     * @param int $id archive id
     * @return bool True if update succeeded, false otherwise
     */
    public function updatePlayedStats(Request $request)
    {
        if($request->ajax()) {
            $id = $request->get('value');
            $archive = Archive::findOrFail($id);
            if (!is_null($archive)) {
                $archive->played += 1;
                $archive->save();
                return response('ok', '200');
            }
        }
        return response('nope', '403');
    }
}
