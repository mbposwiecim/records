<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Archive extends Model implements SluggableInterface
{
    use SluggableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'aired',
        'published',
        'category_id',
    ];

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
        'on_update'  => true,
    ];

    /**
     * Set the archive's category_id
     *
     * @param string $value
     * @return string
     */
    public function setCategoryIdAttribute($value)
    {
        $this->attributes['category_id'] = $value ?: null;
    }

    /**
     * Get archive category
     *
     * @return @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
