// diabsle gulp notification
process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 // production by default
elixir.config.production = false;

// don't generate .map files
elixir.config.sourcemaps = false;

elixir(function(mix) {
    var jquery = 'node_modules/jquery/dist/jquery.min.js';
    var materializecss = 'node_modules/materialize-css/dist';
    var amplitude = 'bower_components/amplitude/js/amplitude.min.js';

    mix.copy(jquery, 'public/js');
    mix.copy(materializecss + '/css/materialize.min.css', 'public/css')
      .copy(materializecss + '/js/materialize.min.js', 'public/js')
      .copy(materializecss + '/fonts', 'public/fonts');
    mix.copy(amplitude, 'public/js');

    mix.sass('app.scss');
    mix.sass('player.scss');
    mix.scripts('app.js');
});
