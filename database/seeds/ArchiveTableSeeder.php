<?php

use Illuminate\Database\Seeder;
use App\Archive;

class ArchiveTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carbon = Carbon\Carbon::now();
        $archive_set = [
            [
                'filepath' => 'http://static.prsa.pl/c8b12d72-67c7-4aa0-bf04-b5c7bd953cd0.mp3',
                'title' => 'Audycja 1',
                'description' => 'Jakiś opis audycji 1',
                'aired' => $carbon->parse('31.03.2016')->toDateString(),
                'published' => $carbon->toDateString()
            ],
            [
                'filepath' => 'http://static.prsa.pl/0a6dffe8-6b54-4623-8dbb-45cc7c0be7b0.mp3',
                'title' => 'Audycja 2',
                'description' => 'Jakiś opis audycji 2',
                'aired' => $carbon->parse('17.03.2016')->toDateString(),
                'published' => $carbon->toDateString()
            ],
            [
                'filepath' => 'http://static.prsa.pl/bdbf2be8-d80b-420e-98e3-69d7c08b52e0.mp3',
                'title' => 'Audycja 3',
                'description' => 'Jakiś opis audycji 3',
                'aired' => $carbon->parse('10.03.2016')->toDateString(),
                'published' => $carbon->toDateString()
            ],
            [
                'filepath' => 'http://static.prsa.pl/8fad4188-c59a-4610-a070-14aecae61b61.mp3',
                'title' => 'Audycja 4',
                'description' => 'Jakiś opis audycji 4',
                'aired' => $carbon->parse('03.03.2016')->toDateString(),
                'published' => $carbon->toDateString()
            ],
            [
                'filepath' => 'http://static.prsa.pl/189b975f-f730-4419-a8ec-a9e07f559a99.mp3',
                'title' => 'Audycja 5',
                'description' => 'Jakiś opis audycji 5',
                'aired' => $carbon->parse('25.02.2016')->toDateString(),
                'published' => $carbon->toDateString()
            ],
        ];

        foreach ($archive_set as $archive) {
            Archive::create($archive);
        }
    }
}
